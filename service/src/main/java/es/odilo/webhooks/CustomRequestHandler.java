package es.odilo.webhooks;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.apache.log4j.Logger;

public abstract class CustomRequestHandler<I, O> implements RequestHandler<I, O> {

    protected final Regions REGION = Regions.EU_WEST_1; // Regions.fromName(System.getenv("AWS_REGION"));
    protected final String STAGE = "dev"; // System.getenv("STAGE");
    protected final String SLACK_TOKEN = "0uFAeVEVfMQrfAOQIC8aj4Tp";

    private final Logger log = Logger.getLogger(CustomRequestHandler.class);
}