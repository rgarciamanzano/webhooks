package es.odilo.webhooks;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.util.json.Jackson;
import es.odilo.ocs.response.ApiResponse;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.util.Map;

public class MessageListener extends CustomRequestHandler<Map<String, Object>, ApiResponse> {

    private final Logger log = Logger.getLogger(MessageListener.class);

    @Override
    public ApiResponse handleRequest(Map<String, Object> input, Context context) {
        BasicConfigurator.configure(); //

        String body = (String) input.get("body");
        String output = Jackson.toJsonPrettyString(body);

        log.info(output);
        return ApiResponse.builder().ok(output);
    }
}